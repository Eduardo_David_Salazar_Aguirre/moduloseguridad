﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QS.CA.ConectaWeb.Entities
{
    [Table ("SEC_USER")]
    public class User
    {
        [Key]
        [Column("Id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("IdUserType")]
        public UserType UserType { get; set; }

        [Column("Name")]
        [StringLength(100)]
        public string Name { get; set; }

        [Column("Email")]
        [StringLength(256)]
        [Required]
        public string Email { get; set; }

        [Column("Country")]
        [StringLength(50)]
        public string Country { get; set; }

        [Column("Department")]
        [StringLength(50)]
        public string Department { get; set; }

        [Column("UserName")]
        [StringLength(256)]
        public string UserName { get; set; }

        [Column("PasswordHash")]
        [StringLength(256)]
        [Required]
        public string PasswordHash { get; set; }

        [Column("AccessFailedCount")]
        public int AccessFailedCount { get; set; }

        [Column("LockoutEndDateUtc")]
        [DataType(DataType.Date)]
        public DateTime LockoutEndDateUtc { get; set; }
        
        [Column("LockoutEnabled")]
        [Required]
        public bool LockoutEnabled { get; set; }

        [Column("Enabled")]
        public bool Enabled { get; set; }

    }
   
}
