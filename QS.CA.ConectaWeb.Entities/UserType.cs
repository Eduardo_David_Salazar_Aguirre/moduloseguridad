﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Entities
{
    [Table("SEC_USER_TYPE")]
    public class UserType
    {
        [Key]
        [Column("Id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("Name")]
        [StringLength(100)]
        public string Name { get; set; }

        [Column("Descripcion")]
        [StringLength(256)]
        public string Descripcion { get; set; }

        [Column("Enable")]
        public bool Enable { get; set; }

    }
}
