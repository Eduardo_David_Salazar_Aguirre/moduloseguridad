﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Entities
{
    [Table("SEC_APPLICATION")]
    public class Application
    {
        [Key]
        [Column("Id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("Name")]
        [StringLength(50)]
        public string Name { get; set; }

        [Column("Description")]
        [StringLength(150)]
        public string Description { get; set; }

        [Column("Enabled")]
        public bool Enabled { get; set; }

        [Column("IdModule")]
        public Module Module { get; set; }

    }
}
