﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Entities
{
    [Table("SEC_PORTAL_AUTHENTICATION")]
    public class PortalAuthentication
    {
        [Key]
        [Column("Id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("ServerIP")]
        [StringLength(250)]
        public string ServerIP { get; set; }

        [Column("ServerUser")]
        [StringLength(50)]
        public string ServerUser { get; set; }

        [Column("ServerPassword")]
        [StringLength(50)]
        public string ServerPassword { get; set; }

        [Column("TokenSecurity")]
        [StringLength(50)]
        public string TokenSecurity { get; set; }

        [Column("IdPortal")]
        public Portal Portal { get; set; }

        [Column("IdAuthentication")]
        public Authentication Authentication { get; set; }
    }
}
