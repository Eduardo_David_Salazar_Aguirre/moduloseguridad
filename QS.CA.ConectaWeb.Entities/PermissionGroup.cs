﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Entities
{
    [Table("SEC_PERMISSION_GROUP")]
    public class PermissionGroup
    {
        [Key]
        [Column("Id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("IdPermission")]
        [Required]
        public Permission Permission { get; set; }

        [Column("IdGroup")]
        [Required]
        public Group Group { get; set; }
    }
}
