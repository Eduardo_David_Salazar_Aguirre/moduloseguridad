﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Entities
{
    [Table("SEC_EVENT_LOG")]
    public class EventLog
    {
        [Key]
        [Column("Id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("Description")]
        [StringLength(150)]
        public string Description { get; set; }
        
        [Column("EventDate")]
        [DataType(DataType.Date)]
        public string EventDate { get; set; }

        [Column("IPAdress")]
        [StringLength(150)]
        public string IPAdress { get; set; }

        [Column("IdEventType")]
        public EventType EventType { get; set; }

        [Column("IdPortal")]
        public Portal Portal { get; set; }

        [Column("IdModule")]
        public Module Module { get; set; }

        [Column("IdApplication")]
        public Application Application { get; set; }

        [Column("IdAction")]
        public Actions Action { get; set; }

        [Column("IdUser")]
        public User User { get; set; }
    }
}
