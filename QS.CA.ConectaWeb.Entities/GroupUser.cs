﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Entities
{
    [Table("SEC_GROUP_USER")]
    public class GroupUser
    {
        [Key]
        [Column("Id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("IdUser")]
        [Required]
        public User User { get; set; }

        [Column("IdGroup")]
        [Required]
        public Group Group { get;  set; }

    }
}
