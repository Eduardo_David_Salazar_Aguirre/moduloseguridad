﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Entities
{
    [Table("SEC_PERMISSION")]
    public class Permission
    {
        [Key]
        [Column("Id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("IdPortal")]
        public Portal Portal { get; set; }

        [Column("IdModule")]
        public Module Module { get; set; }

        [Column("IdApplication")]
        public Application Application { get; set; }

        [Column("IdAction")]
        public Actions Action { get; set; }

        [Column("Enabled")]
        public bool Enabled { get; set; }

    }
}
