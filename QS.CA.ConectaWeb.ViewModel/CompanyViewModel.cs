﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.ViewModel
{
    public class CompanyViewModel
    {
        public int Id { get; set; }
      
        public string Name { get; set; }

        public string Description { get; set; }

        public bool Enabled { get; set; }
    }
}
