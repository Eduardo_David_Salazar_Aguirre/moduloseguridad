﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.ViewModel
{
    public class NewUserViewModel
    {
        [Required]
        public string UserName { get; set; }
        public string Name { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }
        public string Country { get; set; }
        public string Department { get; set; }
    }

    public class UserViewModel
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Country { get; set; }
        public string Department { get; set; }
      
    }
}
