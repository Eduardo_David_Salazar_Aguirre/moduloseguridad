﻿using AutoMapper;
using QS.CA.ConectaWeb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.ViewModel.Mapping
{
    public sealed class MapperConfig
    {
        private MapperConfig() { }

        public static void Configure()
        {
            Mapper.Initialize((cfg) =>
            {
                cfg.CreateMap<User,LoginUserViewModel>().ReverseMap();
            });
        }
    }
}
