﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.ViewModel
{
    public class PortalViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool Enabled { get; set; }

        public string Url { get; set; }

        public string Port { get; set; }

        public int IdCompany { get; set; }
    }
}
