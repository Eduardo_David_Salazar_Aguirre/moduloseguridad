﻿using QS.CA.ConectaWeb.Services.Contracts;
using QS.CA.ConectaWeb.Services;
using Unity;
using System.Data.Entity;
using Unity.Lifetime;
using System.Web ;
using System.Web.Http;
using Unity.AspNet.WebApi;
using QS.CA.ConectaWeb.Model;
using QS.CA.ConectaWeb.Business.Contracts;
using QS.CA.ConectaWeb.Business;

namespace QS.CA.ConectaWeb.IoC
{
    public sealed class ConectaWebContainer
    {
        public static void Create(HttpConfiguration configuration)
        {
            var container = new UnityContainer();
            #region Services
                container.RegisterType<IUserServices, UserService>();
                container.RegisterType<IUserBusiness, UserBusiness>();
                container.RegisterType<ILoginService, LoginService>();
                container.RegisterType<ILoginBusiness, LoginBusiness>();
                container.RegisterType<ICompanyService, CompanyService>();
                container.RegisterType<ICompanyBusiness, CompanyBusiness>();
                container.RegisterType<IPortalService, PortalService>();
                container.RegisterType<IPortalBusiness, PortalBusiness>();
            #endregion

            #region  Singletons
            container.RegisterType<DbContext, ConectaWebContext>(new PerResolveLifetimeManager());
            #endregion

            configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}
