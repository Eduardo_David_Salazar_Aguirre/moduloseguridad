﻿using QS.CA.ConectaWeb.Entities;
using QS.CA.ConectaWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Business.Contracts
{
    public interface IUserBusiness
    {
        List<NewUserViewModel> GetUsers();
        bool CreateUser(NewUserViewModel User);
        string Json(string UserName);
        UserViewModel GetById(int id);
        bool Delete(int id);
        bool Update(UserViewModel user);
    }
}
