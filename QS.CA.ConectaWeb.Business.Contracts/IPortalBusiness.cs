﻿using QS.CA.ConectaWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Business.Contracts
{
    public interface IPortalBusiness
    {
        bool Create(PortalViewModel Portal);
    }
}
