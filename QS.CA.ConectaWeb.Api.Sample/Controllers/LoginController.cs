﻿using QS.CA.ConectaWeb.Business.Contracts;
using QS.CA.ConectaWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace QS.CA.ConectaWeb.Api.Sample.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/out")]
    public class LoginController : ApiController
    {
        readonly ILoginBusiness ULogin;

        public LoginController(ILoginBusiness ULogin)
        {
            this.ULogin = ULogin;
        }

        [HttpGet]
        [Route("echoping")]
        public IHttpActionResult EchoPing()
        {
            return Ok(true);
        }

        [HttpGet]
        [Route("prueba")]
        public IHttpActionResult prueba()
        {
            return Ok(this.ULogin.pruebaencrip("1"));
        }

        [HttpPost]
        [Route("token")]
        public IHttpActionResult Login(LoginUserViewModel user)
        {
            if (this.ULogin.LoginDb(user))
            {
                return Ok(this.ULogin.TokenJWT(user));
            }
            else
            {
                return Unauthorized();
            }
            
        }

        [HttpPost]
        [Route("TokenAll")]
        public IHttpActionResult AllLogin(LoginUserViewModel user)
        {
            if (this.ULogin.AllLogin(user))
            {
                return Ok(this.ULogin.TokenJWT(user));
            }
            else
            {
                return Unauthorized();
            }
        }

    }
}
