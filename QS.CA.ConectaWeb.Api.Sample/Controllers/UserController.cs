﻿using QS.CA.ConectaWeb.Business.Contracts;
using QS.CA.ConectaWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace QS.CA.ConectaWeb.Api.Sample.Controllers
{
    [Authorize] 
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        readonly IUserBusiness user;

        public UserController(IUserBusiness user)
        {
            this.user = user;
        }

        [HttpGet]
        [Route("echoping")]
        public IHttpActionResult EchoPing()
        {
            return Ok(true);
        }

        #region CRUD
        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(NewUserViewModel user)
        {
            var result = this.user.CreateUser(user);
            return Ok(result);
        }

        [HttpPut]
        [Route("update/{id:int}")]
        public IHttpActionResult Update (UserViewModel user)
        {
            var result = this.user.Update(user);
            return Ok(result);
        }

        [HttpDelete]
        [Route("delete/{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            return Ok(this.user.Delete(id));
        }

        [HttpGet]
        [Route("GetById/{id:int}")]
        public IHttpActionResult GetById(int id)
        {
            return Ok(this.user.GetById(id));
        }
        #endregion

        [HttpGet]
        [Route("permisos")]
        public IHttpActionResult Permisos()
        {
            var identity = ((ClaimsIdentity)User.Identity).FindFirst(ClaimTypes.Name);
            var UserName = identity.Value;
            return Ok(this.user.Json(UserName));
        }


    }
}
