﻿using QS.CA.ConectaWeb.Business.Contracts;
using QS.CA.ConectaWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace QS.CA.ConectaWeb.Api.Sample.Controllers
{
    [Authorize]
    [RoutePrefix("api/Company")]
    public class CompanyController : ApiController
    {
        readonly ICompanyBusiness Company;

        public CompanyController(ICompanyBusiness Company)
        {
            this.Company = Company;
        }

        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(CompanyViewModel Company)
        {
            var result = this.Company.Create(Company);
            return Ok(result);
        }

    }
}
