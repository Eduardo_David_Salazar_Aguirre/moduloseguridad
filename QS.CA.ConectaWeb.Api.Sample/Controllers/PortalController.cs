﻿using QS.CA.ConectaWeb.Business.Contracts;
using QS.CA.ConectaWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace QS.CA.ConectaWeb.Api.Sample.Controllers
{
    [Authorize]
    [RoutePrefix("api/Portal")]
    public class PortalController : ApiController
    {
        readonly IPortalBusiness portal;

        public PortalController(IPortalBusiness portal)
        {
            this.portal = portal;
        }

        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(PortalViewModel Portal)
        {
            var result = this.portal.Create(Portal);
            return Ok(result);
        }
    }
}
