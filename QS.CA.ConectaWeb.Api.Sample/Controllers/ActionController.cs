﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace QS.CA.ConectaWeb.Api.Sample.Controllers
{
    [Authorize]
    [RoutePrefix("api/action")]
    public class ActionController : ApiController
    {
        // GET: api/Action
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Action/5
        [Route("getById/{id:int}")]
        public string Get(int id)
        {
            return "value";
        }
        [Route("post")]
        // POST: api/Action
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Action/5
        [Route("put")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Action/5
        [Route("delete")]
        public void Delete(int id)
        {
        }
    }
}
