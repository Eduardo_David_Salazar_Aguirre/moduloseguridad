﻿using QS.CA.ConectaWeb.Business.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace QS.CA.ConectaWeb.Api.Sample.Controllers
{
    public class ValuesController : ApiController
    {
        IUserBusiness userBussines;

        public ValuesController(IUserBusiness userBussines)
        {
            this.userBussines = userBussines;
        }

        // GET api/values
        public IHttpActionResult Get()
        {
            var response = this.userBussines.GetUsers();
            return Ok(response);
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public IHttpActionResult Post()
        {
            //var response = this.userBussines.CreateUser();
            return Ok();
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
