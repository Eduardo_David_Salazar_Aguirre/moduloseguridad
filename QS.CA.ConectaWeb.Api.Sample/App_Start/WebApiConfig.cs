﻿using QS.CA.ConectaWeb.IoC;
using System.Web.Http;
using System.Web.Http.Cors;
using QS.CA.ConectaWeb.Api.Sample.Controllers;

namespace QS.CA.ConectaWeb.Api.Sample
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuración y servicios de API web

            // Rutas de API web
            var cors = new EnableCorsAttribute("http://localhost:4200", "*", "*");

            config.EnableCors(cors);
            config.MapHttpAttributeRoutes();
            config.MessageHandlers.Add(new TokenValidationHandler());
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            ConectaWebContainer.Create(config);
        }
    }
}
