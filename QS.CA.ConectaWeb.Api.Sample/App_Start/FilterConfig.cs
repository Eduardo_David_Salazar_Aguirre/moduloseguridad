﻿using System.Web;
using System.Web.Mvc;

namespace QS.CA.ConectaWeb.Api.Sample
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
