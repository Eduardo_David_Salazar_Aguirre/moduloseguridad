﻿using QS.CA.ConectaWeb.Business.Contracts;
using QS.CA.ConectaWeb.Entities;
using QS.CA.ConectaWeb.Services.Contracts;
using QS.CA.ConectaWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Business
{
    public class UserBusiness : IUserBusiness
    {
        private IUserServices userService;

        public UserBusiness(IUserServices userService)
        {
            this.userService = userService;
        }

        public List<NewUserViewModel> GetUsers()
        {
            return this.userService.GetUsers();
        }

        public bool CreateUser(NewUserViewModel user)
        {
            this.userService.CreateUser(user);
            return true;
        }

        public string Json(string UserName)
        {
            return this.userService.Json(UserName);
        }

        public UserViewModel GetById(int id)
        {
            return this.userService.GetById(id);
        }

        public bool Delete(int id)
        {
            return this.userService.Delete(id);
        }

        public bool Update(UserViewModel user)
        {
            return this.userService.Update(user);
        }
    }
}
