﻿using QS.CA.ConectaWeb.Business.Contracts;
using QS.CA.ConectaWeb.Services.Contracts;
using QS.CA.ConectaWeb.Utilities;
using QS.CA.ConectaWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Business
{
    public class LoginBusiness: ILoginBusiness
    {
        private ILoginService loginService;
        public LoginBusiness(ILoginService loginService)
        {
            this.loginService = loginService;
        }
        public bool LoginDb(LoginUserViewModel user)
        {
            return loginService.LoginDb(user);
        }

        public bool AllLogin(LoginUserViewModel user)
        {
            var dbresul = loginService.LoginDb(user);

            if (!dbresul)
            {
                var adresul = loginService.LoginAD(user);

                if (adresul)
                {
                    return true;
                }
            }
            return false;
        }


        public string pruebaencrip(string cadena)
        {
            return Encrypt.GetSHA256(cadena);
        }

        public string TokenJWT(LoginUserViewModel user)
        {
            return TokenGenerator.GenerateTokenJwt(user.UserName);
        }
    }
}
