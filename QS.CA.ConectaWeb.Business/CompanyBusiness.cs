﻿using QS.CA.ConectaWeb.Business.Contracts;
using QS.CA.ConectaWeb.Services.Contracts;
using QS.CA.ConectaWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Business
{
    public class CompanyBusiness : ICompanyBusiness
    {
        private ICompanyService companyService;

        public CompanyBusiness(ICompanyService companyService)
        {
            this.companyService = companyService;
        }

        public bool Create(CompanyViewModel Company)
        {
           return this.companyService.Create(Company);
        }
    }
}
