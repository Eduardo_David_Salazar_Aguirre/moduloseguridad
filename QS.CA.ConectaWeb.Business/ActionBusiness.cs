﻿using QS.CA.ConectaWeb.Business.Contracts;
using QS.CA.ConectaWeb.Services.Contracts;
using QS.CA.ConectaWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Business
{
    public class ActionBusiness : IActionBusiness
    {
        private IActionService actionService;

        public ActionBusiness(IActionService actionService)
        {
            this.actionService = actionService;
        }

        public bool Create(ActionViewModel action)
        {
            return this.actionService.Create(action);
        }

        public bool Delete(int id)
        {
            return this.actionService.Delete(id);
        }

        public ActionViewModel GetById(int id)
        {
            return this.actionService.GetById(id);
        }

        public bool Update(ActionViewModel action)
        {
            return this.actionService.Update(action);
        }
    }
}