﻿using QS.CA.ConectaWeb.Business.Contracts;
using QS.CA.ConectaWeb.Services.Contracts;
using QS.CA.ConectaWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Business
{
    public class PortalBusiness : IPortalBusiness
    {
        private IPortalService  portalService;
        
        public PortalBusiness(IPortalService portalService)
        {
            this.portalService = portalService;
        }

        public bool Create(PortalViewModel Portal)
        {
            return this.portalService.Create(Portal);
        }
    }
}
