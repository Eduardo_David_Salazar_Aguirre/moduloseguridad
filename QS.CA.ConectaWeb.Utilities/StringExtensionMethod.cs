﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Utilities
{
    public static class StringExtensionMethod
    {
        public static  bool IsNull(string text)
        {
            return string.IsNullOrEmpty(text);
        }
    }
}
