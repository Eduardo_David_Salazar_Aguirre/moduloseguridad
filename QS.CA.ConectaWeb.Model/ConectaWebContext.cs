namespace QS.CA.ConectaWeb.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using QS.CA.ConectaWeb.Entities;
    using System.Data.Entity.Migrations.History;

    [DbConfigurationType(typeof(ModelConfiguration))]
    public partial class ConectaWebContext : DbContext
    {
        public ConectaWebContext()
            : base("name=ConectaWeb")
        {
        }

        public virtual IDbSet<User> User { get; set; }
        public virtual IDbSet<UserType> UserType { get; set; }
        public virtual IDbSet<Company> Company { get; set; }
        public virtual IDbSet<Portal> Portal { get; set; }
        public virtual IDbSet<Module> Module { get; set; }
        public virtual IDbSet<Application> Application { get; set; }
        public virtual IDbSet<Actions> Actions { get; set; }
        public virtual IDbSet<Authentication> Authentications { get; set; }
        public virtual IDbSet<EventType> EventTypes { get; set; }
        public virtual IDbSet<EventLog> EventLogs { get; set; }
        public virtual IDbSet<Group> Groups { get; set; }
        public virtual IDbSet<GroupUser> GroupUsers { get; set; }
        public virtual IDbSet<Permission> Permissions { get; set; }
        public virtual IDbSet<PermissionGroup> PermissionGroups { get; set; }
        public virtual IDbSet<PermissionUser> PermissionUsers { get; set; }
        public virtual IDbSet<PortalAuthentication> PortalAuthentications { get; set; }


        protected override void OnModelCreating(DbModelBuilder ApplicationDbContext)
        {
            Database.SetInitializer<ConectaWebContext>(null);
           // ApplicationDbContext.Entity<HistoryRow>().ToTable(tableName: "MigrationHistorySEC");
            base.OnModelCreating(ApplicationDbContext);
            // Se agrega el usuario con el que se hizo la autenticacion ala base de oracle en mayusculas
            ApplicationDbContext.HasDefaultSchema("ADMINCA".ToUpper());


        }
    }
}
