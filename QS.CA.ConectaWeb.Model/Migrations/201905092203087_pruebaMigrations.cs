namespace QS.CA.ConectaWeb.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pruebaMigrations : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "ADMINCA.SEC_ACTION",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 150),
                        Enabled = c.Decimal(nullable: false, precision: 1, scale: 0),
                        Application_Id = c.Decimal(precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("ADMINCA.SEC_APPLICATION", t => t.Application_Id)
                .Index(t => t.Application_Id);
            
            CreateTable(
                "ADMINCA.SEC_APPLICATION",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 150),
                        Enabled = c.Decimal(nullable: false, precision: 1, scale: 0),
                        Module_Id = c.Decimal(precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("ADMINCA.SEC_MODULE", t => t.Module_Id)
                .Index(t => t.Module_Id);
            
            CreateTable(
                "ADMINCA.SEC_MODULE",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 150),
                        Version = c.String(maxLength: 10),
                        Enabled = c.Decimal(nullable: false, precision: 1, scale: 0),
                        Portal_Id = c.Decimal(precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("ADMINCA.SEC_PORTAL", t => t.Portal_Id)
                .Index(t => t.Portal_Id);
            
            CreateTable(
                "ADMINCA.SEC_PORTAL",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 150),
                        Url = c.String(maxLength: 250),
                        Port = c.String(maxLength: 50),
                        Enabled = c.Decimal(nullable: false, precision: 1, scale: 0),
                        Company_Id = c.Decimal(precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("ADMINCA.SEC_COMPANY", t => t.Company_Id)
                .Index(t => t.Company_Id);
            
            CreateTable(
                "ADMINCA.SEC_COMPANY",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 150),
                        Enabled = c.Decimal(nullable: false, precision: 1, scale: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "ADMINCA.SEC_AUTHENTICATION",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 150),
                        Enabled = c.Decimal(nullable: false, precision: 1, scale: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "ADMINCA.SEC_EVENT_LOG",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Description = c.String(maxLength: 150),
                        EventDate = c.String(),
                        IPAdress = c.String(maxLength: 150),
                        Action_Id = c.Decimal(precision: 10, scale: 0),
                        Application_Id = c.Decimal(precision: 10, scale: 0),
                        EventType_Id = c.Decimal(precision: 10, scale: 0),
                        Module_Id = c.Decimal(precision: 10, scale: 0),
                        Portal_Id = c.Decimal(precision: 10, scale: 0),
                        User_Id = c.Decimal(precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("ADMINCA.SEC_ACTION", t => t.Action_Id)
                .ForeignKey("ADMINCA.SEC_APPLICATION", t => t.Application_Id)
                .ForeignKey("ADMINCA.SEC_EVENT_TYPE", t => t.EventType_Id)
                .ForeignKey("ADMINCA.SEC_MODULE", t => t.Module_Id)
                .ForeignKey("ADMINCA.SEC_PORTAL", t => t.Portal_Id)
                .ForeignKey("ADMINCA.SEC_USER", t => t.User_Id)
                .Index(t => t.Action_Id)
                .Index(t => t.Application_Id)
                .Index(t => t.EventType_Id)
                .Index(t => t.Module_Id)
                .Index(t => t.Portal_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "ADMINCA.SEC_EVENT_TYPE",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 150),
                        Enabled = c.Decimal(nullable: false, precision: 1, scale: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "ADMINCA.SEC_USER",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Name = c.String(maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 256),
                        Country = c.String(maxLength: 50),
                        Department = c.String(maxLength: 50),
                        UserName = c.String(maxLength: 256),
                        PasswordHash = c.String(nullable: false, maxLength: 256),
                        AccessFailedCount = c.Decimal(nullable: false, precision: 10, scale: 0),
                        LockoutEndDateUtc = c.DateTime(nullable: false),
                        LockoutEnabled = c.Decimal(nullable: false, precision: 1, scale: 0),
                        Enabled = c.Decimal(nullable: false, precision: 1, scale: 0),
                        UserType_Id = c.Decimal(precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("ADMINCA.SEC_USER_TYPE", t => t.UserType_Id)
                .Index(t => t.UserType_Id);
            
            CreateTable(
                "ADMINCA.SEC_USER_TYPE",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Name = c.String(maxLength: 100),
                        Descripcion = c.String(maxLength: 256),
                        Enable = c.Decimal(nullable: false, precision: 1, scale: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "ADMINCA.SEC_GROUP",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Name = c.String(maxLength: 50),
                        Description = c.String(maxLength: 150),
                        Enabled = c.Decimal(nullable: false, precision: 1, scale: 0),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "ADMINCA.SEC_GROUP_USER",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Group_Id = c.Decimal(nullable: false, precision: 10, scale: 0),
                        User_Id = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("ADMINCA.SEC_GROUP", t => t.Group_Id, cascadeDelete: true)
                .ForeignKey("ADMINCA.SEC_USER", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Group_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "ADMINCA.SEC_PERMISSION_GROUP",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Group_Id = c.Decimal(nullable: false, precision: 10, scale: 0),
                        Permission_Id = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("ADMINCA.SEC_GROUP", t => t.Group_Id, cascadeDelete: true)
                .ForeignKey("ADMINCA.SEC_PERMISSION", t => t.Permission_Id, cascadeDelete: true)
                .Index(t => t.Group_Id)
                .Index(t => t.Permission_Id);
            
            CreateTable(
                "ADMINCA.SEC_PERMISSION",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Enabled = c.Decimal(nullable: false, precision: 1, scale: 0),
                        Action_Id = c.Decimal(precision: 10, scale: 0),
                        Application_Id = c.Decimal(precision: 10, scale: 0),
                        Module_Id = c.Decimal(precision: 10, scale: 0),
                        Portal_Id = c.Decimal(precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("ADMINCA.SEC_ACTION", t => t.Action_Id)
                .ForeignKey("ADMINCA.SEC_APPLICATION", t => t.Application_Id)
                .ForeignKey("ADMINCA.SEC_MODULE", t => t.Module_Id)
                .ForeignKey("ADMINCA.SEC_PORTAL", t => t.Portal_Id)
                .Index(t => t.Action_Id)
                .Index(t => t.Application_Id)
                .Index(t => t.Module_Id)
                .Index(t => t.Portal_Id);
            
            CreateTable(
                "ADMINCA.SEC_PERMISSION_USER",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Permission_Id = c.Decimal(nullable: false, precision: 10, scale: 0),
                        User_Id = c.Decimal(nullable: false, precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("ADMINCA.SEC_PERMISSION", t => t.Permission_Id, cascadeDelete: true)
                .ForeignKey("ADMINCA.SEC_USER", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Permission_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "ADMINCA.SEC_PORTAL_AUTHENTICATION",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        ServerIP = c.String(maxLength: 250),
                        ServerUser = c.String(maxLength: 50),
                        ServerPassword = c.String(maxLength: 50),
                        TokenSecurity = c.String(maxLength: 50),
                        Authentication_Id = c.Decimal(precision: 10, scale: 0),
                        Portal_Id = c.Decimal(precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("ADMINCA.SEC_AUTHENTICATION", t => t.Authentication_Id)
                .ForeignKey("ADMINCA.SEC_PORTAL", t => t.Portal_Id)
                .Index(t => t.Authentication_Id)
                .Index(t => t.Portal_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("ADMINCA.SEC_PORTAL_AUTHENTICATION", "Portal_Id", "ADMINCA.SEC_PORTAL");
            DropForeignKey("ADMINCA.SEC_PORTAL_AUTHENTICATION", "Authentication_Id", "ADMINCA.SEC_AUTHENTICATION");
            DropForeignKey("ADMINCA.SEC_PERMISSION_USER", "User_Id", "ADMINCA.SEC_USER");
            DropForeignKey("ADMINCA.SEC_PERMISSION_USER", "Permission_Id", "ADMINCA.SEC_PERMISSION");
            DropForeignKey("ADMINCA.SEC_PERMISSION_GROUP", "Permission_Id", "ADMINCA.SEC_PERMISSION");
            DropForeignKey("ADMINCA.SEC_PERMISSION", "Portal_Id", "ADMINCA.SEC_PORTAL");
            DropForeignKey("ADMINCA.SEC_PERMISSION", "Module_Id", "ADMINCA.SEC_MODULE");
            DropForeignKey("ADMINCA.SEC_PERMISSION", "Application_Id", "ADMINCA.SEC_APPLICATION");
            DropForeignKey("ADMINCA.SEC_PERMISSION", "Action_Id", "ADMINCA.SEC_ACTION");
            DropForeignKey("ADMINCA.SEC_PERMISSION_GROUP", "Group_Id", "ADMINCA.SEC_GROUP");
            DropForeignKey("ADMINCA.SEC_GROUP_USER", "User_Id", "ADMINCA.SEC_USER");
            DropForeignKey("ADMINCA.SEC_GROUP_USER", "Group_Id", "ADMINCA.SEC_GROUP");
            DropForeignKey("ADMINCA.SEC_EVENT_LOG", "User_Id", "ADMINCA.SEC_USER");
            DropForeignKey("ADMINCA.SEC_USER", "UserType_Id", "ADMINCA.SEC_USER_TYPE");
            DropForeignKey("ADMINCA.SEC_EVENT_LOG", "Portal_Id", "ADMINCA.SEC_PORTAL");
            DropForeignKey("ADMINCA.SEC_EVENT_LOG", "Module_Id", "ADMINCA.SEC_MODULE");
            DropForeignKey("ADMINCA.SEC_EVENT_LOG", "EventType_Id", "ADMINCA.SEC_EVENT_TYPE");
            DropForeignKey("ADMINCA.SEC_EVENT_LOG", "Application_Id", "ADMINCA.SEC_APPLICATION");
            DropForeignKey("ADMINCA.SEC_EVENT_LOG", "Action_Id", "ADMINCA.SEC_ACTION");
            DropForeignKey("ADMINCA.SEC_ACTION", "Application_Id", "ADMINCA.SEC_APPLICATION");
            DropForeignKey("ADMINCA.SEC_APPLICATION", "Module_Id", "ADMINCA.SEC_MODULE");
            DropForeignKey("ADMINCA.SEC_MODULE", "Portal_Id", "ADMINCA.SEC_PORTAL");
            DropForeignKey("ADMINCA.SEC_PORTAL", "Company_Id", "ADMINCA.SEC_COMPANY");
            DropIndex("ADMINCA.SEC_PORTAL_AUTHENTICATION", new[] { "Portal_Id" });
            DropIndex("ADMINCA.SEC_PORTAL_AUTHENTICATION", new[] { "Authentication_Id" });
            DropIndex("ADMINCA.SEC_PERMISSION_USER", new[] { "User_Id" });
            DropIndex("ADMINCA.SEC_PERMISSION_USER", new[] { "Permission_Id" });
            DropIndex("ADMINCA.SEC_PERMISSION", new[] { "Portal_Id" });
            DropIndex("ADMINCA.SEC_PERMISSION", new[] { "Module_Id" });
            DropIndex("ADMINCA.SEC_PERMISSION", new[] { "Application_Id" });
            DropIndex("ADMINCA.SEC_PERMISSION", new[] { "Action_Id" });
            DropIndex("ADMINCA.SEC_PERMISSION_GROUP", new[] { "Permission_Id" });
            DropIndex("ADMINCA.SEC_PERMISSION_GROUP", new[] { "Group_Id" });
            DropIndex("ADMINCA.SEC_GROUP_USER", new[] { "User_Id" });
            DropIndex("ADMINCA.SEC_GROUP_USER", new[] { "Group_Id" });
            DropIndex("ADMINCA.SEC_USER", new[] { "UserType_Id" });
            DropIndex("ADMINCA.SEC_EVENT_LOG", new[] { "User_Id" });
            DropIndex("ADMINCA.SEC_EVENT_LOG", new[] { "Portal_Id" });
            DropIndex("ADMINCA.SEC_EVENT_LOG", new[] { "Module_Id" });
            DropIndex("ADMINCA.SEC_EVENT_LOG", new[] { "EventType_Id" });
            DropIndex("ADMINCA.SEC_EVENT_LOG", new[] { "Application_Id" });
            DropIndex("ADMINCA.SEC_EVENT_LOG", new[] { "Action_Id" });
            DropIndex("ADMINCA.SEC_PORTAL", new[] { "Company_Id" });
            DropIndex("ADMINCA.SEC_MODULE", new[] { "Portal_Id" });
            DropIndex("ADMINCA.SEC_APPLICATION", new[] { "Module_Id" });
            DropIndex("ADMINCA.SEC_ACTION", new[] { "Application_Id" });
            DropTable("ADMINCA.SEC_PORTAL_AUTHENTICATION");
            DropTable("ADMINCA.SEC_PERMISSION_USER");
            DropTable("ADMINCA.SEC_PERMISSION");
            DropTable("ADMINCA.SEC_PERMISSION_GROUP");
            DropTable("ADMINCA.SEC_GROUP_USER");
            DropTable("ADMINCA.SEC_GROUP");
            DropTable("ADMINCA.SEC_USER_TYPE");
            DropTable("ADMINCA.SEC_USER");
            DropTable("ADMINCA.SEC_EVENT_TYPE");
            DropTable("ADMINCA.SEC_EVENT_LOG");
            DropTable("ADMINCA.SEC_AUTHENTICATION");
            DropTable("ADMINCA.SEC_COMPANY");
            DropTable("ADMINCA.SEC_PORTAL");
            DropTable("ADMINCA.SEC_MODULE");
            DropTable("ADMINCA.SEC_APPLICATION");
            DropTable("ADMINCA.SEC_ACTION");
        }
    }
}
