﻿using QS.CA.ConectaWeb.Entities;
using QS.CA.ConectaWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Services.Contracts
{
    public interface IUserServices
    {
        List<NewUserViewModel> GetUsers();
        bool CreateUser(NewUserViewModel user);
        string Json(string UserName);
        UserViewModel GetById(int id);
        bool Delete(int id);
        bool Update(UserViewModel user);
    }
}
