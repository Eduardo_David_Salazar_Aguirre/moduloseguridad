﻿using QS.CA.ConectaWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Services.Contracts
{
    public interface ILoginService
    {
        bool LoginDb(LoginUserViewModel user);
        bool ApLogin(LoginUserViewModel user);
        bool LoginAD(LoginUserViewModel user);
    }
}
