﻿using QS.CA.ConectaWeb.Model;
using QS.CA.ConectaWeb.Services.Contracts;
using QS.CA.ConectaWeb.ViewModel;
using QS.CA.ConectaWeb.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Services
{
    public class PortalService : IPortalService
    {
        ConectaWebContext dbContext;

        public PortalService (DbContext dbContext)
        {
            this.dbContext = (ConectaWebContext)dbContext;
        }

        public bool Create(PortalViewModel Portal)
        {
            try
            {
                var company = (from c in dbContext.Company.AsQueryable()
                           where c.Id == Portal.IdCompany
                           select c
                            ).FirstOrDefault();

                var aux = new Portal
                {
                    Id = 0,
                    Name = Portal.Name,
                    Description = Portal.Description,
                    Url = Portal.Url,
                    Port = Portal.Port,
                    Company = company,
                    Enabled = Portal.Enabled
                };
            
                    this.dbContext.Portal.Add(aux);
                    this.dbContext.SaveChanges();

                    return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
