﻿using QS.CA.ConectaWeb.Entities;
using QS.CA.ConectaWeb.Model;
using QS.CA.ConectaWeb.Services.Contracts;
using QS.CA.ConectaWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Services
{
    public class ActionService : IActionService
    {
        private ConectaWebContext dbContext;

        public ActionService(DbContext dbContext)
        {
            this.dbContext = (ConectaWebContext)dbContext;
        }

        public bool Create(ActionViewModel action)
        {
            try
            {
                var app = dbContext.Application.SingleOrDefault(a => a.Id == action.IdApplication);

                this.dbContext.Actions.Add(new Actions
                {
                    Id = 0,
                    Name = action.Name,
                    Description = action.Description,
                    Enabled = action.Enabled,
                    Application = app
                });
                this.dbContext.SaveChanges();
                return true;
            }catch(Exception ex)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var action = dbContext.Actions.SingleOrDefault(a=> a.Id == id);
                if (action != null)
                {
                    dbContext.Actions.Remove(action);
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public ActionViewModel GetById(int id)
        {
            try
            {
                var actionv = (from act in dbContext.Actions.AsQueryable()
                               where act.Id == id
                               select new ActionViewModel
                               {
                                   Id = act.Id,
                                   Name = act.Name,
                                   Description = act.Description,
                                   Enabled = act.Enabled,
                                   IdApplication = act.Application.Id
                               }).FirstOrDefault();
                return actionv;
            }catch(Exception ex)
            {
                return null;
            }
        }

        public bool Update(ActionViewModel action)
        {
            try
            {
                var actionUpdate = dbContext.Actions.Single(a => a.Id == action.Id);
                var aplication = dbContext.Application.Single(a => a.Id == action.IdApplication);

                actionUpdate.Name = action.Name;
                actionUpdate.Description = action.Description;
                actionUpdate.Application = aplication;
                actionUpdate.Enabled = action.Enabled;
                dbContext.Actions.Add(actionUpdate);
                dbContext.SaveChanges();

                return true;
                
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
