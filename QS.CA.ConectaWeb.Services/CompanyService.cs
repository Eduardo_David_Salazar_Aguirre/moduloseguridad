﻿using QS.CA.ConectaWeb.Services.Contracts;
using QS.CA.ConectaWeb.ViewModel;
using QS.CA.ConectaWeb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QS.CA.ConectaWeb.Model;
using System.Data.Entity;

namespace QS.CA.ConectaWeb.Services
{
    public class CompanyService : ICompanyService
    {
        ConectaWebContext dbContext;

        public CompanyService (DbContext dbContext)
        {
            this.dbContext = (ConectaWebContext)dbContext;
        }

        public bool Create(CompanyViewModel Company)
        {
            var aux = new Company
            {
                Id = 0,
                Name = Company.Name,
                Description = Company.Description,
                Enabled = Company.Enabled
            };
            try
            {
                this.dbContext.Company.Add(aux);
                this.dbContext.SaveChanges();

                return true;

            } catch (Exception ex)
            {
                return false;
            }

        }
    }
}
