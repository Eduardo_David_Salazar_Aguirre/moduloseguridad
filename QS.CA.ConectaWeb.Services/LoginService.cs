﻿using QS.CA.ConectaWeb.Model;
using QS.CA.ConectaWeb.Services.Contracts;
using QS.CA.ConectaWeb.Utilities;
using QS.CA.ConectaWeb.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QS.CA.ConectaWeb.Services
{
    public class LoginService : ILoginService
    {
        ConectaWebContext dbContext;

        public LoginService(DbContext dbContext)
        {
            this.dbContext = (ConectaWebContext)dbContext;
        }

        public bool LoginDb(LoginUserViewModel user)
        {

            string username = user.UserName;
            string password = Encrypt.GetSHA256(user.Password);

            try
            {
                var VUser = (from u in this.dbContext.User
                             where u.UserName == username && u.PasswordHash == password
                             select u
                            ).FirstOrDefault();

                return VUser != null ? true : false;
            }
            catch (Exception ex)
            {

                return false;
            }


        }

        public bool LoginAD(LoginUserViewModel user)
        {
            try
            {
                var portal = (from p in dbContext.Portal.AsQueryable()
                              where p.Id == user.PortalID
                              select p
                              ).FirstOrDefault();


                string acceso = portal.Url + @"\" + user.UserName;
                string dominio = "LDAP://" + portal.Url;
                //DirectoryEntry entry = new DirectoryEntry("LDAP://PruebasCDMX.com", user.UserName, user.Password);
                DirectoryEntry entry = new DirectoryEntry(dominio, user.UserName, user.Password);
                entry.AuthenticationType = AuthenticationTypes.Secure;

                object obj = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = "(SAMAccountName=" + user.UserName + ")";
                string[] requiredProperties = new string[] { "cn", "givenname", "sn" };
                foreach (String property in requiredProperties)
                    search.PropertiesToLoad.Add(property);

                SearchResult result = search.FindOne();

                return result != null ? true : false;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool ApLogin(LoginUserViewModel user)
        {
            int? IdApp = user.PortalID;
            if(IdApp != null)
            {
                bool App = true; // (from a in this.dbContext.)

                if (App)
                {
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        return false;
                    }

                    return true;
                }
                else
                {
                    return false;
                }

             
            }
            else
            {
                return false;
            }
        }
    }
}
