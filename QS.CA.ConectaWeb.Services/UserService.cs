﻿using Newtonsoft.Json;
using QS.CA.ConectaWeb.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using QS.CA.ConectaWeb.Model;
using QS.CA.ConectaWeb.ViewModel;
using QS.CA.ConectaWeb.Utilities;
using System.Security.Claims;
using QS.CA.ConectaWeb.Entities;

namespace QS.CA.ConectaWeb.Services
{
    public class UserService : IUserServices
    {
        private ConectaWebContext dbContext;

        public UserService(DbContext dbContext)
        {
            this.dbContext = (ConectaWebContext)dbContext;
        }

        public List<NewUserViewModel> GetUsers()
        {
            List<NewUserViewModel> x = new List<NewUserViewModel>();
            return x;
        }
        
        public string Json(string UserName)
        {
            string[] permisos = { "A", "qui", "lista"};
            var json = JsonConvert.SerializeObject(
                new
                {
                    userName = "Holis",
                    fullName = "Aqui toy",
                    email = "Holis@holis.com",
                    permisos
                }
                );
            return json;
        }

        #region Basic CRUD 

        public bool CreateUser(NewUserViewModel user)
        {
            try
            {
                var passhash = Encrypt.GetSHA256(user.Password);

                var aux = new User
                {
                    Id = 0,
                    UserName = user.UserName,
                    Email = user.Email,
                    Department = user.Department,
                    Country = user.Country,
                    PasswordHash = passhash,
                    Name = user.Name,
                    LockoutEnabled = true
                };

                this.dbContext.User.Add(aux);
                this.dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public UserViewModel GetById(int id)
        {
            var usuario = (from u in dbContext.User.AsQueryable()
                           where u.Id == id
                           select new UserViewModel
                           {
                               id = u.Id,
                               Name = u.Name
                           }).FirstOrDefault();
            return usuario;
        }

        public bool Delete(int id)
        {
            try
            {
                var usuario = (from u in dbContext.User.AsQueryable()
                               where u.Id == id
                               select u).FirstOrDefault();
                if (usuario != null)
                {

                    this.dbContext.User.Remove(usuario);
                    this.dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Update(UserViewModel user)
        {
            try
            {
                var userupdate = dbContext.User.SingleOrDefault(p => p.Id == user.id);

                userupdate.Name = user.Name;
                userupdate.Country = user.Country;
                userupdate.Department = user.Department;

                dbContext.User.Add(userupdate);
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            
        }

        #endregion 
    }
}
